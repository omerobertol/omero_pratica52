import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * 
 * @author Omero Francisco Bertol <chicobertol@gmail.com>
*/

public class Pratica52 {
    
  public static void main(String[] args) {
    Equacao2Grau iguais = new Equacao2Grau(2, 4, 2);           
    Equacao2Grau dif = new Equacao2Grau(1, 4, 3);
    Equacao2Grau nao = new Equacao2Grau(1, 1, 1);
    
    try {
      System.out.println("1a. raiz = " + dif.getRaiz1());
      System.out.println("2a. raiz = " + dif.getRaiz2());
      System.out.println();
      System.out.println("1a. raiz = " + iguais.getRaiz1());
      System.out.println("2a. raiz = " + iguais.getRaiz2());
      System.out.println();
      System.out.println("1a. raiz = " + nao.getRaiz1());
      System.out.println("2a. raiz = " + nao.getRaiz2());
    } catch (RuntimeException err) {
        System.out.println("Erro: " + err.getLocalizedMessage());
      }  
  }
    
}
