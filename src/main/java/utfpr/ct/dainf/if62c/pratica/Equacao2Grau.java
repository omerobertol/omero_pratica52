package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 * @param <T>
 */

public class Equacao2Grau<T extends Number> {
  private T a;
  private T b;
  private T c;

  public Equacao2Grau(T a, T b, T c) {
    if (a.doubleValue() == 0)
       throw new RuntimeException("Coeficiente a não pode ser zero");
    
    this.a = a;
    this.b = b;
    this.c = c;
  }
   
  public void setA(T a) {
    if (a.doubleValue() == 0)
       throw new RuntimeException("Coeficiente a não pode ser zero");
    
    this.a = a;
  }

  public void setB(T b) {
    this.b = b;
  }

  public void setC(T c) {
    this.c = c;
  }

  public T getA() {
    return a;
  }
  
  public T getB() {
    return b;
  }

  public T getC() {
    return c;
  }
  
  public double getRaiz1() {
    double raiz = Math.pow(this.b.doubleValue(), 2) - (4 * this.a.doubleValue() * this.c.doubleValue());
    double result;
    
    // (b ao quadrado − 4ac) <0 a raíz não existem  
    if (raiz < 0)  
       throw new RuntimeException("Equação não tem solução real");        
    
    result = (-this.b.doubleValue() - Math.sqrt(raiz)) / (2 * this.a.doubleValue());
    
    return result;
  }
  
  public double getRaiz2() {
    double raiz = Math.pow(this.b.doubleValue(), 2) - (4 * this.a.doubleValue() * this.c.doubleValue());
    double result;
    
    // (b ao quadrado − 4ac) <0 a raíz não existem  
    if (raiz < 0)  
       throw new RuntimeException("Equação não tem solução real");      
    
    result = (-this.b.doubleValue() + Math.sqrt(raiz)) / (2 * this.a.doubleValue());
    
    return result;
  }
  
}